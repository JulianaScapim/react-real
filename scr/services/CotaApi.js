import axios from 'axios'

const api = axios.create({
    baseURL: "https://economia.awesomeapi.com.br/"
})

function moeda(setCoin) {
    api.get("/json/all")
        .then( response => {
            let lis = []
            Object.keys(response.data).forEach(key => {
                lis.push(response.data[key])
            })
            setCoin(lis)
            
        }).catch( error => {
            console.log(error)
    })
}

export {moeda}