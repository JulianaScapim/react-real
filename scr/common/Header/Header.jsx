import React from 'react'
import {Link} from 'react-router-dom'
import './Header.css'

function Header() {
    return (
        <header>
            <nav id='lin'>
                <Link to="/Cota">Cota      </Link>
                <br/>
                <Link to="/">Home </Link>
            </nav>
        </header>
    )
}

export default Header