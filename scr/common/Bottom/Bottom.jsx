import {Link} from 'react-router-dom'
import './Bottom.css'
import { createBrowserHistory } from "history" 
const history = createBrowserHistory({ 
    basename: "/" 
}) 
window.redirect = history.push

function Bottom() {
    return (
        <a href="/"><button>Voltar</button></a>
    ) 
}

export default Bottom