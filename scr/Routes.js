import React from 'react'
import Cota from './pages/Cota/Cota'
import Home from './pages/Home/Home'
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'
import NotFound from './pages/NotFound/NotFound'
import Construcao from './pages/Construcao/Construcao'

function Routes() {
    return(
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    <Home/>
                </Route>

                <Route exact path="/Cota">
                    <Cota/>
                </Route>

                <Route exact path="/Construcao">
                    <Construcao/>
                </Route>

                <Route path="/">
                    <NotFound />
                </Route>
            </Switch>        
        </BrowserRouter>
    )
}

export default Routes