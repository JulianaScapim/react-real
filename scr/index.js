import React from 'react';
import ReactDOM from 'react-dom';
import Home from './App';
// import Erro from './services/NotFound';
// import construcao from './services/Construcao';
// import cotacao from './services/Cotacao';


ReactDOM.render(
  <React.StrictMode>
    <Home/>
  </React.StrictMode>,

  document.getElementById('root')
);
