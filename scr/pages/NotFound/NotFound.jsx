import React from 'react'
import foto1 from './../../content/fotos/confuso.svg';
import Header from '../../common/Header/Header';
import Footer from '../../common/Footer/Footer';
import Bottom from '../../common/Bottom/Bottom';
import './Not.css';

function NotFound() {
    return(
        <>
        <Header/>
        <main>
        <header id="retangulo">
                <nav>
                    <a> <h1 id= 'p3'>Erro 404</h1></a>
                    <img src={foto1} id="footer-foto1" alt="foto1" />
                    <p id='p2'>Ops! Parece que o endereço que você digitou está incorreto...</p>
                    <Bottom/>
                </nav>
                </header>
        </main>
        <Footer/>
        </>
    )
}

export default NotFound