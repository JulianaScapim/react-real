import foto1 from './../../content/fotos/porquinho.svg';
import foto2 from './../../content/fotos/pensa.svg';
import foto3 from './../../content/fotos/escada.svg';
import foto4 from './../../content/fotos/ajuda.svg';
import foto5 from './../../content/fotos/cofre.svg';
import './Home.css';
import Header from '../../common/Header/Header';
import Footer from '../../common/Footer/Footer';
import Butao from './Butao/Butao';
import {Link} from 'react-router-dom'; 

function Home() {
  return (
      <>
      <Header/>
      <div className="Home">
      {/* Texto porquinho */}
      {/* Foto porquinho */}
        <header id="retangulo1">
          <nav id="menu">
              <a> <h1>Ping Bank</h1>
              <h4 id='inicio'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a leo eu enim auctor faucibus id eget dolor. Pellentesque faucibus leo sed arcu vulputate rhoncus. </h4>
              <Butao/>
              </a>
              <img src={foto1} className="Home-foto1" alt="foto1" />
          </nav>
        </header>
        {/* Parte meio */}
        <section id="retangulo2">
            <h1 id='fonte'>Como usar</h1>
            <ul class="row">
                <li class="coluna">
                    <figure>
                        <img src={foto2} className="foto" alt="foto2" />
                        <h4>DEFINA UMA META</h4>
                        <h5>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</h5>
                    </figure>
                </li>
                <li class="coluna">
                    <figure>
                        <img src={foto3} className="foto" alt="foto3" />
                        <h4>ACOMPANHE SEU PROGRESSO</h4>
                        <h5>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</h5>
                    </figure>
                </li>
                <li class="coluna" >
                    <figure>
                        <img src={foto4} className="foto" alt="foto4" />
                        <h4 class="centro">ALCANCE SEU OBJETIVO</h4>
                        <h5>Pellentesque faucibus leo sed arcu vulputate rhoncus. Integer sit amet blandit felis.</h5>
                    </figure>
                </li>
            </ul> 
        </section>  
        {/* Fim */}
        <header id="retangulo1">
          <nav id="menu">
              <a> <h1 id='fonte'>Já tem uma meta?</h1>
              <h5>Phasellus mollis eget enim sed malesuada. Nulla facilisi. Aliquam nunc lacus, commodo hendrerit commodo pulvinar, suscipit eget ipsum. Nulla at sem ex.</h5>
              </a>
              <img src={foto5} className="Home-foto1" alt="foto1" />
          </nav>
        </header>
        <Butao/>
      </div>
      <Footer/>
      </>
  );
}
export default Home