import React, { useEffect, useState } from 'react'
import Header from '../../common/Header/Header'
import Footer from '../../common/Footer/Footer'
import { moeda } from '../../services/CotaApi'
import './Cota.css'

function Money() {
    const [coins, setCoin] = useState([])

    useEffect(() => {
        moeda(setCoin)
    }, [])
    useEffect(() => {
        console.log(coins)
    }, [coins])

    return(
        <>
            <Header/>
            <main>
            {coins.map(din => {
                return(
                    <div id = 'caixa'>
                        <h1 id='nome'>{din.name}<br/>
                        {din.code}<br/>
                         Máxima : {din.high}<br/>
                         Mínima : {din.low}<br/>
                        {din.create_date}</h1><br/>
                    </div>
                )
            })}
            </main>
            <Footer/>   
        </>
    )
}
export default Money